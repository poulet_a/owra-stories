# Le baron de Penthièvre

## Chapitre 1: Le village de Tarffidd

- arrivée au village après avoir escorté un marchand (4 jours après).
- rencontre avec Morgane qui leur révèle l'assassinat de 2 inconnus à proximité du manoir

## Chapitre 2: Des indices sous les tapis

- arrivée au manoir le jour suivant, des anneaux de guerre ont été volés. Enquête qui mène à la piste de documents volés à propos d'un convois qui doit arriver le lendemain

## Chapitre 3: Du sang pour de l'or

- ils rejoignent le convois le lendemain, peu avant l'attaque
- ils remarquent un mec en noir qui a l'air louche
- ils tuent 1 voleur, en capturent 1 autre et les 5 autres s'enfuient
- soupçons sur Luc
- arrivée en soirée au village

## Chapitre 4: Retour au cimetière

- retour au manoir
- le père et clarence ont été blessés, bjar est mort, à cause d'assassins pendant la nuit. Luc a disparu pendant l'attaque
- ils trouvent ensuite 2 cadavres très frais (deux hommes qui accompagnaient le convoi jusqu'au manoir
- ils retrouvent Morgane en plein combat contre le mercenaire en noir qui voulait passer jusqu'à la chambre du baron et le tuent