# Partie 4 : La colone

### Chapitre 1 : Shön
Départ depuis Arkan en mission d'escorte auprès d'une caravane marchande. Arrivée à Shön après deux jours de trajet.

### Chapitre 2 : Camping à la belle étoile
Discussion déplacée avec un contremaître le la scierie et avec le taverniers. Ces deux rencontres ont faillit se transformer en bagarre, mais les aventuriers sur les dents finissent par s'éloigner sous la contrainte.
Départ vers le sud-ouest et camping dehors à la belle étoile.

### Chapitre 3 : Un gros problèmes
Arrêt 40 kilomètres plus loin, et la forêt est traversée.
Des torches (vu par Amaryllis qui s'est transformée en aigle. Il s'agit de réfugiés en provenance des royaumes du sud.)
Le matin, ils se dirigent vers les réfugiés mais un géant attaque. Il le traquent dans la montagne.

### Chapitre 4 : Un trou béant
Ils combattent le géant.
Oberyn et Amaryllis combattent durement, et Norbak blessé.
Le géant, gravement blessé, bluffe en tombant au sol et attrape Rufus qu'il lance sur Oberyn. Il est achevé par le groupe.
En connaisseurs, le groupe, mené par Oberyn, cherche la tanière sûrement remplie de trésors.
Après quelques dizaines de mètres dans la grotte, ils tombent sur une tribu de troglodytes qui leur tombe dessus.