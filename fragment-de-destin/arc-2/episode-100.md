# Introduction

## Chapitre 1: L'assaut

Léon (chevalier) est à la taverne du village.
Tharja est endormie dans sa chambre.
Faraday arrive à la taverne, prend timidement une assiette avant d'être harrassée par 3 locaux.
Elle sort de ses gonds au bout de quelques minutes et Tharja, reveillée, descend se plaindre avec force.
Les cors orcs résonnent au loin.

## Chapitre 2: Première ligne

Pendant que les villageois et Tharja fuient vers les montagnes, Léon et Faraday, assistés des gardes repoussent les premiers assaillants orcs. Les soldats et Léon fuient ensuite, suivis de Faraday.
La colone arrive aux cavernes fortifiées pendant la nuit.

## Chapitre 3: Caves sombres

Le lendemain matin, le village trouve un jeune homme assassiné. Thraja identifie que la mort est due à une créature ou un genre de vampire. Les 3 aventuriers se rendent ensuite dans les cavernes profondes pour y retrouver l'herboriste.

Ils arrivent dans une caverne colossale où Faraday retrouve l'herboriste. Ils reviennent au camp après avoir vu une lueur bleu étrange et une nuée de chauve-souris.