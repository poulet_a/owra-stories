# Partie 12 : Le Roi

## Chapitre 1 : Retour en ville

Rufus et Tharja, après quelques jours de voyage, arrivent enfin à Arkan.
Rufus se dirige immédiatement chez la baronne Elisabeth, pensant y retrouver ses compagnons.
Elle les reçoit en soirée, et les informe que leurs compagnons ne sont pas encore passés la voir, mais que d'après ses informateurs ils sont à Öm.
Elle les empresse d'aller les rejoindre, leur disant qu'elle a de bonnes nouvelles à leur annoncer, mais qu'elle préfère attendre qu'ils soient tous réunis.
Elle prête aux deux aventurier deux de ses chevaux le lendemain matin, après leur avoir proposé l'hospitalité pour la nuit, afin faciliter leur voyage jusqu'à Öm, leur demandant de les lui rendre le lendemain.

## Chapitre 2 : Tanuki et disparition

De leur côté, Oberyn et Amaryllis sont attablés à la taverne de Öm.
Durant leur repas, ils croisent Bard et ses compagnons, des aventuriers qu'Amaryllis avait rencontré par le passé, et ceux-ci se dirigent amicalement vers la table des deux aventuriers.
Amarylis montre leur en vouloir pour ses actes lors de leur précédente rencontre, mais Bard n'en tient pas compte et les informe sèchement du sort du Tanuki que lui et sa compagnie poursuivent depuis maintenant deux semaines : il avait mis le feu à plusieurs village et massacré des innocents.
Il retourne ensuite à ses compagnons, laissant Amaryllis et Oberyn seuls.

Amaryllis, l'air toujours aussi maussade depuis quelques jours, informe Oberyn de son intention de quitter le groupe, car elle refuse de continuer à être le pion d'humains.

Le soir, le haut prêtre Salomon étant censé être rentré, ils retournent au temple afin de tenir conseil avec lui.
Il n'est cependant toujours pas là, mais son assistant, le père Goriot, donne à Oberyn une missive à son attention.
Il est expliqué dans cette courte lettre cachetée que des affaires le retardaient, mais qu'il serait de retour à Öm sans faute le lendemain matin.
Il est également indiqué dans la lettre qu'Oberyn serait le prince des Ukwards, un nom qui ne lui dit rien.
Oberyn et Amaryllis vont se coucher, l'un dans la taverne et l'autre dans la forêt proche, en attendant le lendemain.

Oberyn se réveille de bonne heure, comme à son habitude.
Il descend dans la salle commune de la taverne pour y prendre son petit-déjeuner en attendant Amaryllis. Il y règne une agitation inhabituelle : beaucoup de personnes sont présentes, dont certaines semblent assez secouées, et tous parlent très fort. Se renseignant auprès du tavernier, il apprend que quelque chose a brûlé plusieurs fermes du village cette nuit là.

Les heures passent et celle-ci, pourtant debout assez tôt d'habitude, ne vient pas.
Il décide donc d'aller la chercher dans le bosquet le plus proche.
Après avoir un peu fouillé, il ne trouve rien, comprenant que la jeune elfe s'était sûrement en allée durant la nuit.

## Chapitre 3 : Les Artefacts

Revenant au village, Oberyn aperçoit deux cavaliers arriver : il reconnaît Rufus, qui est accompagné d'une jeune femme qu'il ne connaît pas, et qui se présente à lui comme s'appelant Tharja. Ensemble, ils vont au temple de Pélor pour rencontrer le haut prêtre Salomon.  
Celui-ci les accueille, s'inclinant devant le nain, et les invite dans son bureau. Il donne à Oberyn des précisions sur sa lettre : Ukward est le nom qui était donné aux nains lors de leur apogée, même si ce nom est maintenant tombé dans l'oubli. Il annonce ensuite aux compagnons qu'il a pu, grâce aux indices qu'il a récupérés sur le corps et dans les possessions de feu Aspecaux, récolter un nombre important d'informations concernant les artéfacts nains.  
Il existe en effet trois artéfacts, et non pas un seul. Ils ont été créés par le sacrifice d"un seigneur humain, Melwin. Lorsqu'il se sacrifia pour protéger les nains, dans le but de permettre ensuite à son peuple de les rejoindre dans la libération, trois artéfacts naquirent de son corps.  
Le premier est le coeur. Celui-ci possède un puissant pouvoir de protection, protégeant un peuple des intentions mauvaises des autres peuples sur un territoire entier. C'est l'artéfact que les aventurier recherchent, et qui a été volé.  
Le deuxième est l'oeil. Cet artéfact a le pouvoir de lire dans les pensées des ennemis de son porteur. Il serait actuellement protégé par le dragon Paarthurnax, dans une montagne au sud.  
Le dernier artéfact est la main. Elle permettrait à son porteur de devenir un forgeron exceptionnel, capable de forger des armes enchantées et terriblement puissantes. Sa position est cependant inconnue, mais elle serait enfouie quelque part sous terre, là où seul Oberyn pourrait y accéder.

## Chapitre 4 : Le sort du prisonnier

Après avoir entendu ces précieuses informations, Rufus presse son compagnon de revenir avec lui voir la baronne, leur commanditaire. Mais Oberyn l'informe alors de la perte de l'artéfact aux mains des voleurs, et Tharja les convainc d'aller interroger plus en profondeur leur otage.  
En rentrant dans la prison, ils voient le garde surveillant la porte endormi. Inquiets quant à l'éventuelle évasion du changeforme, ils se dépêchent de rejoindre sa cellule. Ils y voient le changeforme, attaché, et le sergent Ferville, très occupé à le questionner. Oberyn réussit à convaincre ce dernier de les laisser, ses compagnons et lui, interroger le prisonnier à leur tour, bien qu'il n'y tienne pas lui-même. Tharja lance alors quelques sorts sur Jack, sans beaucoup de succès, puis Rufus et elle décident alors d'emprunter une voie plus diplomatique. Après de nombreuses minutes de négociations, ils arrivent à un marché : le changeforme leur dirait où se trouve la pierre, puis les y conduirait, et ils le libèreraient ensuite. Selon lui, la pierre n'était pas entre les mains de sa guilde, mais il l'avait évidemment toujours sur lui lorsqu'Amaryllis et Oberyn l'avaient capturé. Ces informations leur semblant convaincantes, ils décident de lui faire confiance et de le sortir de la prison pour le laisser les guider. Mais alors que Rufus et Oberyn argumentent pour savoir si oui ou non, le voleur devrait être handicapé afin de limiter ses chances de fuite, Tharja coupe court à la discussion en lui sectionnant les tendons d'achille.  
Oberyn convainc alors Ferville, non sans mal, de les laisser prendre le voleur avec eux, et ils se mettent en route. Mais avant de quitter le village, voulant demander à Salomon s'il a plus d'informations concernant leur otage, ils font un détour par le temple. Le haut prêtre n'est malheureusement plus là, et seul son assistant est présent. Rufus et Tharja tentent alors de s'infiltrer dans le bureau de leur allié, mais le père Goriot s'en aperçoit et les chasse du temple.

## Chapitre 5 : Le Coeur de Melwin

Les trois compagnons, ainsi que leur otage, arrivent à Arkan dans la soirée. Ils vont immédiatement chez la baronne. Celle-ci les informe alors qu'elle a en fait récupéré l'artéfact, qui était en effet en possession du voleur lorsqu'il a été capturé, et qui était tombé par terre à ce moment. Ayant fait suivre le groupe tout au long de leur périple, ses hommes ont pu le récupérer grâce à leurs actions. Le Coeur est désormais chez le duc, dont les mages essaient de comprendre le fonctionnement. Elle demande à Oberyn, héritier des artéfacts, s'il leur en veut d'avoir récupéré l'artéfact sans son accord, mais ce dernier lui répond qu'il préférait qu'il soit entre des mains alliées. Elle lui dit ensuite que le duc devrait le contacter sous peu, pour sa cérémonie d'intronisation.  
Elle leur donne ensuite une récompenses en pierres précieuses que Rufus et Oberyn se partagent. Elle leur promet aussi de chercher des objets magiques à leur donner pour compléter leur récompense.

## Chapitre 6 : Un nouveau roi

Les trois compagnons rentrent ensuite à l'auberge, où ils retrouvent Kulay, le fidèle compagnon de Rufus, qui l'y attend depuis plusieurs jours. Ils réservent une chambre, mangent et vont dormir. Le lendemain matin, ils se font réveiller en grande pompe par l'apparition spectaculaire d'un mage, qui se présente comme un messager du duc, et invite Oberyn à sa cérémonie d'intronisation trois heures plus tard. Il disparaît ensuite. Les trois se lèvent et flânent un peu en ville, avant de se rendre au palais.  
Une fois arrivés, ils rencontrent deux autres nains, les chefs des deux plus grands clans nains de la région. Oberyn reconnaît l'un d'eux, un vieil ami de son oncle. Il s'avance ensuite vers le duc, qui le reconnaît en tant que roi des nains, et lui offre son alliance et son aide pour reconstruire son royaume.  
Ses deux compagnons sont ensuite appelés, pour avoir aidé à retrouver l'artéfact, et reçoivent également une récompense - une pierre précieuse et des broches magiques.  
Après la cérémonie, un bal est donné, où Rufus entreprend non sans succès de faire la cour à la baronne. Tharja disparaît pendant ce temps, préférant profiter de la nuit avec le duergar.