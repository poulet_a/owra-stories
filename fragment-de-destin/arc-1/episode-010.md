# Partie 10 : Le Changeforme

## Chapitre 1 : Le cavalier

Amaryllis, toujours perchée sur une des scierie, remarque en scrutant le paysage un cavalier partant au galop de la ville.
Convaincue qu'il s'agit du voleur, elle s'en va alerter ses compagnons.

Elle va retrouver Oberyn, qui l'informe que Rufus est parti précipitamment pour une affaire urgente et importante.
Après avoir fouillé la prison, les deux compagnons et Kulay sortent affronter la foule pour rejoindre la scierie, où Amaryllis avait repéré des chevaux.

## Chapitre 2 : La foule et les éléments

A peine sortis du bâtiment, le groupe se retrouve face aux émeutiers. Si certains reconnaissent Oberyn, ce n'est pas le cas de tous, et ils sont rapidement encerclés et attaqués par les migrants.
Après quelques tentatives infructueuses de les faire reculer, Amaryllis décide de faire appel aux éléments.
Le ciel se couvre rapidement de nuages, et un vent glacial se met à souffler dans le village, précédant une violente chute de flocons de neige.

Profitant de cette opportunité, les compagnons tentent de s'avancer de nouveau, mais le vent et la neige bloquent leurs mouvements et ils peinent à se frayer un passage à travers la foule.
Se concentrant de nouveau, Amaryllis donne vie aux vents de cette tempête et invoque un élémentaire d'air.
A peine invoqué, celui-ci se change en une violente tornade et fonce dans la foule devant le groupe, leur ouvrant une voie.
Après quelques dizaines de secondes, les aventuriers sortent enfin de la masse, laissant derrière eux des émeutiers désorientés.

## Chapitre 3 : La course-poursuite

Arrivés près de la scierie, les trois compagnons retrouvent Arnaud, qui a récupéré des chevaux.
Il leur annonce avoir envoyé un messager à Owyl pour demander des renforts pour mater la foule en colère.
N'ayant que deux chevaux, Amaryllis et Oberyn décident de se séparer de Kulay, qui leur offre de les rejoindre à Arkan.
Ce qu'il reste du groupe prend alors la route, galopant dans les ténèbres.
Épuisée, Amaryllis profite de l'occasion pour se reposer sur le cheval d'Oberyn.

## Chapitre 4 : La capture

Après avoir chevauché toute la nuit, les deux aventuriers rattrapent enfin le voleur, qu'Oberyn reconnaît comme étant Norbak.
Amaryllis, le voyant plutôt comme un monstre, prévient le nain et lui demande de l'attaquer.
L'homme qui leur fait face, nerveux, change d'apparence, avant de leur annoncer qu'il n'a pas la relique, et que Norbak, leur ami, est en danger de mort s'ils ne rebroussent pas chemin pour aller l'aider.
Amaryllis et Oberyn refusent cependant de le laisser s'échapper, et décident de le capturer.

Après avoir essuyé des tirs d'arbalète de la place du nain, le changeforme prend la fuite sur son cheval, malgré les tentatives d'Amaryllis pour convaincre ce dernier de trahir son maître.
Le voyant tenter de s'enfuir, celle-ci s'élance, attrapant la dague d'Oberyn au passage, et tente de saboter sa selle pour le faire tomber. Elle n'y arrive cependant pas entièrement, le cuir étant trop solide, et l'homme en profite pour lui planter une dague dans l'épaule.
Lâchant prise sous l'effet de la douleur, Amaryllis tombe au sol, achevant avec sa chute épique la sangle de la selle.

Le voleur, manquant de tomber de son cheval, est forcé de ralentir la cadence, ce qui permet à Oberyn, qui a profité de ces quelques moments pour monter sur un de leurs propres chevaux, de le rattraper.
Après un combat acharné entre les deux hommes pour mettre l'autre hors course, Oberyn sort vainqueur, et fait tomber le voleur au sol.
Après un dernier échange de sorts et de projectiles, le changeforme se rend et accepte d'être fait prisonnier par le groupe.

## Chapitre 5 : Retrouvailles

Le groupe, accompagné désormais de leur prisonnier soigneusement attaché par Oberyn, fait demi-tour et, suivant les directions du voleur, arrivent au bosquet où Norbak a été déposé.
En discutant avec le changeforme, ils apprennent qu'il appartient à un groupe ayant son quartier général à Elgil, et que la relique serait bientôt en leur possession.

Avant de s'enfoncer dans les buissons du bosquet, Oberyn assomme le prisonnier, craignant qu'il ne s'échappe.
Après avoir fait quelques pas, les deux compagnons se retrouvent cependant bloqués, s'étant tous les deux pris dans des pièges à ours.
Amaryllis se transforme en arbre pour briser le piège, puis rejoint Oberyn et tente de l'aider.
Après de nombreuses tentatives infructueuses qui ont failli coûter au nain sa jambe, il est enfin libéré du piège.
Ils rejoignent ensuite le corps de Norbak, gisant au milieu du bosquet.
Son état est grave.
Affamé, déshydraté, brûlé au fer rouge, les ongles arrachés, il est stabilisé par les soins magiques intensifs d'Amaryllis.
Cependant, cela ne suffit pas à ce qu'il se réveille, tant les blessures et les tortures qu'il a subies sont importantes.

Le groupe décide alors de se reposer, n'étant pas en état de continuer plus loin.

## Chapitre 6 : Retour à Öm

Après avoir passé la journée à dormir et panser leurs blessures, les aventuriers reprennent la route, cette fois plus tranquillement.
En se dirigeant vers Arkan, ils croisent une caravane de marchands transportant des villageois rescapés de Shön.
Ils décident de les accompagner, glanant au passage quelques nouvelles du village - l'inquisiteur avait effectivement purgé le village des migrants, avec l'aide d'autres guerriers.
Et les discussions à voix basse des rescapés laissaient entendre une campagne d'intimidation et de terreur importante, incluant de nombreux empalements.
Arrivés à Arkan, ils décident de faire le chemin restant sans prendre le temps de se poser, l'état de leur ami étant critique et le temps pressant.

Arrivés à Öm, ils confient Norbak aux prêtres de Pélor et leur prisonnier au sergent Fervil.
Ils discutent un peu avec le sergent, lui donnant les quelques informations qu'ils ont sur ce qui est arrivé à Norbak, puis vont prendre un repos - et des choppes de bière - bien mérité.