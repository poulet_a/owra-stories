# Partie 7 : Le temple

### Chapitre 1 : Sortir d'ici
Trouvent la source de pollution/poison (forge primaire) et la bloque.
Cherchent un moyen de partir alors que tout est bloqué (glace, éboulements, énigme). Prises de têtes entre les aventuriers. Idée de l'air chaud.

### Chapitre 2 : Vestige
Combat contre des moines nains-rocheux.
Ils arrivent à l'air libre.

### Chapitre 3 : Le village
Une ombre dans le ciel. Un troll géant attrape Rufus, mais il est sauvé par Oberyn, et le troll prend la fuite à cause du feu créé par Amaryllis.
Ils retrouvent Norbak pendant la nuit en le trouvant au camps.
Pendant la nuit, Amaryllis accompagne la fée à son arbre qui est mort. Elle reste donc avec eux.
Le lendemain, ils prennent la route et trouvent une forêt partiellement incendiée.
Ils y pénétrent et découvrent les restes d'un village.

### Chapitre 4 : Qui êtes-vous ?
Amaryllis Oberyn et Norbak descendent dans le puis. Ils trouvent un bassin d'eau pure avec une plaque en acier au fond. Ils remontent ensuite.
Pendant ce temps, Rufus trouvent les corps des derniers villageois grace au loup, ainsi que leurs déformations et quelques bribes d'histoire.
Amaryllis et Norbak trouvent la grotte que la baronne leur avait indiqué, pendant qu'Oberyn et Rufus creusent une fosse commune pour les corps.

### Chapitre 5 : Sur les traces du passé
Le groupe descend dans la caverne. Ils trouvent 2 portent vers le temple et vers le cimetière.
Ils finissent après 2 heures par trouver des interstices où Oberyn met ses doigts et ouvre la porte vers le temple. Ils retournent au niveau du bassin précédent, l'ouvrent et le vide de son eau.
Ils arrivent dans une caverne grossière avec 2 couloirs parallèles. Ils y sont arrêtés par un élémentaire de terre. Ils essaient de ruser mais ce dernier les attaque. Après un combat compliqué, ils éliminent l'élémentaire. Ils arrivent enfin sur le temple dans lequel une immense statue représentant un héro nain anonyme.

### Chapitre 6 : Ténèbres
La statue se trouve être un gardien avec qui Oberyn parvient à discuter. Ils doit prouver qu'il est digne d'être l'héritier d'Angham. Pour cela, il doit "repousser les ténébres".
Ils descendent par une porte secrete dans une chambre sacrée circulaire où le sol s'est partiellement effondré. En dessous, un couloir mène à une tanière remplie d'araignées géantes.
Le groupe parvient à mettre en déroute une demi-douzaine, tuant une araignée ogre monstrueuse et ses rejetons, après un rude combat durant lequel ils détruisent une nuée d'araignée à l'aide de feu magique et dans lequel Rufus et Amaryllis sont grièvement blessés.