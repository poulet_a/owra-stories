# Introduction

### Chapitre 1 : La mine

Sortie de la mine

### Chapitre 2 : Du courage sous le soleil

Discussion pour chercher ou pas les 2 autres.
Soleil étrange.

### Chapitre 3 : Des pas dans les ténèbres

Descente dans la mine.
Impossible de passer.
Combat avec 3 gobelins.
Retour en haut.

### Chapitre 4 : Soleil de printemps

Voyage jusqu'à Öm.
Rencontre avec Fervil.
La prière de midi.

### Chapitre 5 : Tout commence et se termine au coin du feu

Discussion avec Salomon.
Départ d'Alexandros.
Arrivée à la taverne.