# Owra

Visitez le wiki sur: [owra.nephos.xyz](https://owra.nephos.xyz/)

- Narré par Nephos.
- Relu et corrigé par Damaia (aka Amaryllis)

## Joué par
- Emeraude (Norbak, Léon, Théodric) : 18 séances
- Shigu (Rufus) : 11 séances
- Damaia (Amaryllis, Tharja, Cordélia) : 23 séances
- Tenkai (Oberyn) : 9 séances
- Faraday (Faraday, Samael): 10 séances
- Morgane (Morgane): 2 séance

(mis à jour à la l'épisode 109)

## [Fragment de destin - Histoire](/pages/fragment-de-destin/)
- [Fragment de destin - Arc 1 - Ukwards awakening](/pages/fragment-de-destin/arc-1/)
- [Fragment de destin - Arc 2 - Rise of the Darkness](/pages/fragment-de-destin/arc-2/)
- [Fragment de destin - Arc 3 - Gates of the Fate](/pages/fragment-de-destin/arc-3/)

### Campagnes secondaires
- [Fragment de destin - Arc 2 - Les Ukwards se lèvent](/fragment-de-destin/arc-2-oav1/)

## Avec les joueurs
- [Personnages principaux](/pages/personnages/)
- [Histoire écrite par Nephos](/pages/fragment-de-destin/)
- [Découvertes des aventuriers](/pages/joueurs/)

![carte](/uploads/carte-owra.jpg)

Autre cartes:

- [Owra, dernière version](/uploads/carte-owra.jpg)
- [Owra, version manuscrite](/uploads/carte-owra-old.jpg)
- [Le domaine des Pentrièvres](/uploads/carte-penthievres.jpg)
