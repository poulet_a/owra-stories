# Diabolistes

## Chapitre 1: chercher leur cachette

- Examen du corps de l'assassin: il s'agit d'une âme tourmentée par des diables
- Les traces des assassins mènent grossièrement vers le nord
- Ophelia se rappel d'un village abandonné il y a longtemps dans le nord

## Chapitre 2: Un mort

- Après une journée de marche, ils arrivent au village (il s'agit d'une clairière avec quelques murs)
- Traces de sang quasi-invisibles qui mènent vers un cadavre torturé
- Symboles infernaux partout, une dague sacrificielle

## Chapitre 3: La crypte

- Le groupe trouve un cimetière en suivant les traces
- Un mausolée semble avoir été ouvert
- À l’intérieur, le groupe tombe sur une crypte souterraine. Ils descendent
- Au bout du couloir souterrain, une porte qui semble gorgée de magie

## Chapitre 4: Les gardiens

- Samael touche la porte avec la dague, ce qui active un piège qui fait fuir le groupe pendant quelques instants.
- Un mur de glace s'est formé derrière le groupe dans le couloir. Passer dedans les blesse à cause du froid magique qui s'en dégage
- Le groupe passe le mur et ouvre la porte
- Derrière 2 diablesse se trouvent et les attaquent presque sans sommation
- Le combat fait rage, et les diablesses invoquent des diables barbus. Le groupe est blessé mais ils en viennent à bout.

## Chapitre 5: Diable osseux

- Le groupe fouille la salle où se trouvaient les diablesses
- Ils découvrent que les corps enterrés ici semblent avoir été marqué il y a longtemps par des diabolistes
- Quand le groupe commence à repartir, une forme apparaît (un diable osseux) dans le cerce au centre de la salle
- Ce dernier les dénigre et les attaque quand le groupe s'enfuit dans le couloir
- Le groupe parvient à remonter mais le diable s'est téléporté devant eux.
- Ils le combattent et enfin face aux aventuriers il finit par être vaincu