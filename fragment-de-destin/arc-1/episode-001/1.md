# Partie 1 : La tête de l'ogre

### Chapitre 1 : Aube

La disparition du chevalier assombrie un petit peu le Fervil.

\- J'ai un certain nombre d'accréditations. De compétences, qui s'étendent plus loin que Öm, et notamment... J'ai la responsabilité de certaines missions à assigner. Des missions pour le Duc.  
\- Une quête ? Questionna Norbak.  
\- Pas exactement. Vous êtes au courant que la frontière sud est défendue contre les tribus orcs par des fortifications, installées à des points stratégiques des monts du fer ? Nous avons repris depuis quelques semaines les constructions. Plusieurs forts sont actuellement mal défendus car pas encore terminés. Le Duc considère que nous avons besoin de plus d'informations et d'être capables de réagir rapidement. C'est pourquoi j'ai reçu un ordre de mission à confier à des aventuriers.  
\- Une petite ballade en montagne quoi. Lâcha alors Amaryllis, contrairement à son mutisme habituel.  
\- Ce sera peut-être un petit peu plus compliqué que cela.  
\- Pourquoi le Duc remonte-t-il de nouvelles fortifications ? Fît le grand blond, d'un air soudainement suspicieux.  
\- Tu l'as compris Norbak. Les orcs se sont réveillés.

---

Fervil marqua une pause.

\- Les éclaireurs ont noté une activité importante chez les tribus orcs principales. Le Duc a déjà envisagé une guerre de plusieurs semaines. Voir plusieurs mois. Et... Cela se rapproche très vite.  

Cela n'était pas bon. Les orcs qui s'activaient, l'absence d'aventuriers dans cette taverne habituellement bondée... Rien de bon pour Norbak qui, cependant, connaissait les prix du Duc.

\- Le contrat est de vingt cinq pièces d'or par personne et par jour d'activité. Du matériel basique vous sera même gracieusement fournit dans les forts en cas de nécessité. Sans compter les primes. Il y a de quoi te permettre de vivre plutôt bien. De plus, j'attends d'autres personnes à cette table.  
\- Comment cela ?
\- Vous n'étiez que trois... Maintenant deux. Cette mission nécessitera peut-être plus de bras.  
\- Je préfère travailler en petit comité, sergent.  
\- Ils ne seront que deux ou trois. Cela ne devrait pas...

À ce moment, deux silhouettes mirent pieds dans la taverne.
La première, de petite taille, peut-être un mètre cinquante, plutôt charnue, semblait être un nain en armure d'écailles. Un lourd bouclier dans le dos et un large glaive à la taille.
La seconde, plus grande, encapuchonnée et drapée d'une cape grise sombre, ne laissait pas voir son visage. Cependant, on pouvait deviner une peau blême et imberbe sans rides sous la capuche.
Les deux inconnus s'approchèrent.

Ils... Elles dévoilèrent leurs noms. Aluna, magicienne, et Sheylis, protectrice naine.
Revenue depuis quelques semaines d'une mission loin dans l'ouest, elles s'étaient installées dans le village pour une semaine de repos.
Le sergent avait donc fait leur connaissance, puis proposé cette mission.
Norbak restait réticent à l'idée de s'associer avec des aventuriers presque inconnus.
D'autant que la façon dont la magicienne se comportait n'avait rien de rassurant.
Cette dernière n'avait pas quitté son capuchon, qu'elle gardait pour cacher son visage avec application.
Mais la soirée s'étirait, et peu à peu, il finit par se rendre compte de la qualité d'un groupe ainsi constitué.
Fervil les quitta en fin de soirée, leur proposant simplement de le retrouver le lendemain matin, avant de partir, contrat signé, vers les fortifications de ce nouveau fort, le "12b".

Quand il eu quitté l'assemblée, une ambiance gênée s'installa.
Rapidement, Amaryllis quitta la table afin de se dégourdir les jambes, et de dormir, comme à son habitude, à la belle étoile.
Sheylis fit de même, prétextant l'envie de dormir, et monta dans la chambre qu'elle avait réservée, laissant Norbak et Aluna, seuls.
Norbak, finissant un bol de ragout, discuta brièvement avec cette dernière, cherchant à mieux cerner la personne.
Mais Aluna, peu loquace, ne fit que répondre par quelques phrases courtes et fermées.
Finalement, le grand blond se leva, et salua.

Aluna se leva peu après la sortie de Norbak, et se dirigea vers l’extérieur pour faire quelques pas.
Un vent du sud, venant des montages, soufflait sur le village, frais et vivifiant.
Elle retira son capuchon, à la lumière vacillante de la lune.
Un gros chat, plus loin, lambinait sur un rebord de fenêtre.
Elle s'approcha, le caressant de ses longs doigts, fin et blancs, comme des os.
Le chat cessa de respirer peu après qu'elle lui ai mordu la gorge, vidant le gros félin de son sang.
S'essuyant sur quelques serviettes qu'elle avait sur elle, et après avoir enterré le corps, elle repartit en direction de la taverne, pour s'y reposer quelques heures.
Elle remis sa capuche, peu avant d'entrer, cachant de nouveau sa peau émaciée et partiellement cireuse.

Le levé fut précoce.
Dès l'aube, alors que les fermiers avaient à peine ouvert leurs volets, le groupe était déjà en train de terminer son petit déjeuné, composé de petites boulettes de pattes, de farine et d'épices.
Fervil avait décidé de se joindre à eux pour boire sa pinte matinale, et sorti le contrat dont il avait été question.

"Vingt-cinq pièces d'or", pensa presque trop fort Norbak.
Une belle somme fixe, même pour un travail dangereux ;
d'autant que les primes que touchaient les aventuriers étaient souvent très intéressantes.

Un contrat avec le Duc n'était pas forcement facile à obtenir, du moins, pas comme le sergent leur faisait signer dès lors.
Il s'agissait d'un contrat total sans limite de durée, très libre, et prometteur de nombreuses missions.
Norbak se promis juste de garder un œil sur cette magicienne.

Après un tour à la forge et chez quelques artisans locaux, afin de renouveler certaines pièces d’équipement, ils prirent la route du sud.
Ils arriveront en début de soirée au fort.