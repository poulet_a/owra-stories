# Bad guys

## Chapitre 1: Le plan

- parlent avec l'herboriste et le chef de la garde
- soin de faraday
- découverte de la nature des créature de la montagne
- plan = se cacher et faire tuer les orcs par les créatures

## Chapitre 2: Execution

- cherchent un grotte sûre
- trouvent un jungle souterraine cachée
- les villageois s'y cachent
- ouverture de la porte

## Chapitre 3: Dead end

- les orcs poursuivent les aventuriers dans la caverne
- l'essaim de créature est réveillé
- les aventuriers se planquent sur des plateformes
- ils tuent les quelques orcs qui les surveillent
- ils retournent chercher les villageois
- Léon est tué par l'essaim car trop lent

## Chapitre 4: La fuite

- les civils et les soldats sont introuvables (plusieurs cadavres dévorés)
- 15-30 orcs festoient et ont quelques prisonniers
- de nuit les deux aventuriers tentent de se faufiler pendant la fête
- ils sont repérés et arrivent à sortir
- des orcs les rattrapent et ils combattent presque jusqu'à en mourir
- ils finissent par fuir assez loin et s'arrête plusieurs kilomètres plus loin près d'un ruisseau
- les deux attendent 