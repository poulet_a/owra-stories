# Trésors des pensées

### Chapitre 1: Arrivée dans la taverne
Arrivée à la taverne sans Norbak. Les bougies sont bizarres, les gens parlent une autre langage, pollution, rues pavées, ...

### Chapitre 2: Découverte de la ville, l'entrée au château
Achat d'une english suit et épée parapluie, bus vers le chateau, gardes

### Chapitre 3: La fête
Passage des gardes. Rencontre avec le baron Flobert. Installation dans les chambres. Visite des docks par Rufus.
Début de la fête et danse entre Rufus et la Duchesse Flare.

### Chapitre 4: Au meurtre
Discours du baron. Discussion avec des convives. Norbak arrive au moment ou le gamin poignarde la duchesse et le plaque. Le lache, le replaque. Les torches de la rébellion sont allumées dehors.
Exorcisme, la créature apparait.

### Chapitre 5: Le donjon
Combat contre la créature. Elle fuit dans une illusion. Poursuite. Affrontement avec 3 gardes dont l'esprit est brisée.
Arrivée en haut d'une tour doté d'un dôme en marbre. Ils détectent le monstre qui leur tend un piège.
Norbak reduit monte, et esquive l'attaque. Le nain arrive et engage le combat. La créature réagit étrangement.
Les deux autre arrivent. Rufus echoue ses sortillèges et Amaryllis sous forme de loup attaque les tentacules pour les occuper.
Rafale de feu qui les touche tous.
Fumée noir partout.
Le nain abat la créature et voit l'enfant à l'interieur.