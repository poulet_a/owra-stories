# Partie 5 : Citée millénaire

### Chapitre 1 : Combat sous-terrain
Norbak est resté dehors pour faire le guet.
Affrontement avec les troglodytes. Le loup et Rufus sont, Oberyn combat avec difficulté les créatures. Amaryllis invoque de monstres sur eux. Oberyn en blesse gravement 3.
Après quelques attaques acides, le loup qui égorge un d'eux et Oberyn tue un fuyard, Rufus achève un d'entre eux, capturé par des toiles de l'araignée invoquée.

### Chapitre 2 : Camping à la belle étoile
Ils se replient pour se reposer quelques heures dehors.
Les réfugiés leur apprennent qu'ils fuyaient la guerre du Sud et des grosses bêtes sanguinaires sur leur route.
Ils vont se reposer dans un bosquet.
Mais pendant la nuit Rufus voit de la lumière dans un buisson.
Il reveille Oberyn et va voir seul.

### Chapitre 3 : Rencontre improbable
Une fée leur dit être perdue et ne pas réussir à rentrer dans sa maison / arbre.
Ils acceptent de l'aider et le lendemain trouvent que le ruisseau à proximité était empoisonné.
Ils remontent à sa source et arrivent devant la grotte.

### Chapitre 4 : Le roi sous la montagne
Description de la grotte.
Discussion hard core avec le RSLM.
Ils fuient face à lui, Oberyn est blessé en aidant Rufus.
Amaryllis essaye de fuir mais retransformée en humaine prend la fuite avec eux et se blesse en tombant.

### Chapitre 5 : La porte
Descente dans les entrailles de la terre. Ils trouvent les forges et explorent les sous-terrains.
Rufus a une prise de tête avec ses compagnons car sa nature de polymorphe reprend le dessus quelques instants.
Ils sont repérés par des nains gris en s'approchant, et ces derniers fuient.
Le groupe se repose devant une porte scellée des nains.

### Chapitre 6 : Reveil difficile
Un nain gris essaie d'assassiner Oberyn mais échoue et se rend invisible pour fuir.
Finalement ils trouvent l'entrée de la porte vers "le village". Ils trouvent comment l'ouvrir.
