# Le vieux village

## Chapitre 1: Fin de bataille

- soins des aventuriers
- l'épine est draconique

## Chapitre 2: Le village

- les chevaliers et les aventuriers (avant garde) vont vers ombreuse
- le lendemain, la troupe est attaquée par des sorcières (1 seul mort chez les chevaliers)
- arrivée au village en ruine en soirée

## Chapitre 3: Dragon

- fouille de l'église en ruine
- passage dans le passé
- combat contre le prêtre dragon
- dehors des sorcières serpentines dansent autour des cadavres d'impériaux empalés
- le prêtre dragon devient un dragon noir
- les aventuriers sortent de l'eglise et reviennent dans le présent

## Chapitre 4: Chapeau Rouge

- le groupe est soigné et dors
- le lendemain ils explorent
- ils vont au pont où ils sont arrêtés par un nain vieux
- ils répondent à son enigme
- le nain les attaque et jete Faraday dans la rivière
- ils finissent par sauver Faraday et faire fuir le chapeau rouge

## Chapitre 5: La bibliotèque

- ils vont vers le sud
- ils trouvent l'elfe gardien de la bibliotèque Ji et vont manger chez lui
- Faraday en apprend plus sur son passé