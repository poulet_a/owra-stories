# Cordelia Fiona Isabella De Penthièvre

## Physique

- Humaine bretteur
- 19 ans, 1m55, 43kg
- Cheveux brun-roux, la plupart du temps attachés
- Yeux verts

## Caractère

- Loyal Neutre
- Capricieuse, sale caractère, très grand sens de l'honneur (surtout du sien)

## Histoire

- Fille d'un noble mineur de Owra
- A passé son enfance à avoir tout ce qu'elle voulait
- A un frère jumeau avec qui elle s'entend bien, malgré quelques rivaleries entre les deux
- Développe assez jeune un goût pour le combat, son père finit par céder et lui faire donner des leçons d'escrime
- Ses penchants guerriers continuent à se développer alors qu'elle grandit, au grand désarroi de son père qui préfèrerait qu'elle se comporte de manière plus féminine
- S'entraîne souvent avec la garde, les accompagnant même parfois en patrouille ou en mission à partir de ses 15 ans
- Son père décide de léguer toute sa baronnie à son frère, alors qu'il est plus jeune (d'une minute) qu'elle
- Cordelia, n'arrivant pas à faire céder son père, quitte la demeure familiale sur un coup de tête et se fait recruter par une guilde d'aventuriers
- Après deux semaines de mission au service du duché, elle reprend contact avec sa famille, et décide avec leur accord de continuer à servir le duché de cette manière, étant donné qu'aucun rôle important ne l'attend chez elle
- Rêve de triomphe et de gloire, pour pouvoir prouver à son père qu'elle vaut tout aussi bien (si ce n'est mieux) que son frère

## Aime :
- Se battre (surtout quand elle gagne)
- Gagner (surtout contre des garçons)
- Qu'on l'admire
- Être la meilleure
- Sa famille (mais ils abusent un peu quand même)
- Sa baronnie et son duché
- Être utile
- La chasse à coure
- Les chevaux
- Son épée
- Avoir la classe
- L'art

## N'aime pas :
- Les garçons
- Ceux qui menacent ce qu'elle aime bien
- Se faire rabaisser
- Se faire sous-estimer
- Se faire contredire
- Ne pas se faire obéir